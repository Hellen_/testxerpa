import React from 'react';

// Material-UI
import { Grid, Hidden } from "@material-ui/core"

// Components
import UserBar from "../../components/userBar/userBar"
import CardItem from "../../components/cardItem/cardItem"

// Styles
import style from "./indexStyle.css"

class Index extends React.Component {
    render() {
        const { infos, starred } = this.props
        return (
            <Grid container  >
                <Grid container direction="row">

                    <Hidden smDown>
                        <Grid item xs={3} lg={2} className={style.containerUserBar} >
                            <UserBar
                                location={infos.location}
                                avatar={infos.avatar_url}
                                name={infos.name}
                                login={infos.login}
                                blog={infos.blog}
                                email={infos.email}
                                bio={infos.bio}
                                company={infos.company}
                            />
                        </Grid>
                    </Hidden>

                    <Grid item xs={12} sm={9} lg={10} className={style.containerCardItems}>
                        {starred.map((item, id) => (
                            < CardItem
                                key={id}
                                full_name={item.full_name}
                                description={item.description}
                                stargazers_count={item.stargazers_count}
                            />
                        ))}
                    </Grid>
                </Grid>
            </Grid>
        );
    }
}

export default Index;