import React from 'react';

// Redux
import { bindActionCreators, compose } from "redux";
import { connect } from "react-redux";
import { toggleHeaderAvatar } from "../../redux/actions/handleHeader"

// Services
import { dataGitHub } from "../../services/api.js"

// Material-UI
import { Grid, Button, Popover, Input, Dialog, DialogTitle, DialogContent, DialogActions } from "@material-ui/core"
import { withStyles } from '@material-ui/core/styles';

// Styles
import style from "./cardItemStyle.css"

const styles = () => ({
    cssRoot: {
        width: "90px",
        color: "white",
        backgroundColor: "#5152B7",
        '&:hover': {
            backgroundColor: "#4b4ca8",
        },
    },
    cssRootOutlined: {
        width: "90px",
        border: "2px solid #5152B7"
    },
    popoverProps: {
        backgroundColor: "black",
        color: "white",
        padding: "15px"
    }
})
class CardItem extends React.Component {
    state = {
        unstar: false,
        openPopover: false,
        openDialog: false,
        errorInput: false,
        tokenUser: "",
        responseAuthUser: [],
        responseStarredRepos: [],
        responseHandleRepo: [],
    }

    toggleButton = () => {
        const { unstar, openPopover, tokenUser, openDialog } = this.state
        const { full_name } = this.props

        if (tokenUser === "") {
            this.setState({
                ...this.state,
                openDialog: !openDialog,
            });
        } else {
            dataGitHub.deleteStarredRepos(full_name, tokenUser).then(function (response) {
                this.setState({
                    ...this.state,
                    responseHandleRepo: response.data
                })
            }.bind(this))
            this.setState({
                ...this.state,
                openPopover: !openPopover,
                unstar: !unstar
            });
        }
        return
    }

    authUserInput = (e) => {
        this.setState({
            ...this.state,
            errorInput: false,
            tokenUser: e.target.value
        });
        return
    }

    authUserLogin = () => {
        const { tokenUser } = this.state
        const { full_name, toggleHeaderAvatar } = this.props
        if (tokenUser !== "") {
            dataGitHub.putStarredRepos(full_name, tokenUser).then(function (response) {
                this.setState({
                    ...this.state,
                    responseHandleRepo: response.data
                })
            }.bind(this))

            dataGitHub.getAuthUser(tokenUser).then(function (response) {
                toggleHeaderAvatar(response.data.avatar_url)
            })

            this.setState({
                ...this.state,
                unstar: true,
                openPopover: true,
                openDialog: false
            })
        } if (tokenUser.length < 40) {
            this.setState({
                ...this.state,
                errorInput: true
            });
        }
        return
    }

    handleClose = () => {
        this.setState({
            openPopover: false,
            openDialog: false
        });
    };

    render() {
        const { unstar, openPopover, openDialog, tokenUser, errorInput } = this.state;
        const { full_name, description, stargazers_count, classes } = this.props;
        return (
            <Grid container direction="row" alignItems="center" className={style.container}  >
                <Dialog
                    open={openDialog}
                    onClose={this.handleClose}
                >
                    <DialogTitle id="form-dialog-title">Login required</DialogTitle>
                    <DialogContent>
                        <Input
                            error={errorInput}
                            onChange={this.authUserInput}
                            value={tokenUser}
                            placeholder="your token"
                            autoFocus
                            id="name"
                            type="email"
                            fullWidth
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={() => this.authUserLogin()} color="primary">
                            Login
                        </Button>
                    </DialogActions>
                </Dialog>

                <Popover
                    PaperProps={{ classes: { root: classes.popoverProps } }}
                    open={openPopover}
                    onClose={this.handleClose}
                    anchorReference="anchorPosition"
                    anchorPosition={{ top: 1000, left: 0 }}
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'left',
                    }}
                    transformOrigin={{
                        vertical: 'bottom',
                        horizontal: 'left',
                    }}
                >
                    {!unstar ? "Repo unstarred!" : "Repo starred with success!"}

                    <Button style={{ color: "gray" }} onClick={() => this.handleClose()}>
                        Dismiss
                    </Button>
                </Popover>
                <Grid item xs={8}>
                    <Grid item style={{ color: "black" }}>
                        {full_name ? full_name.replace("/", " / ") : null}
                    </Grid>
                    <Grid item >
                        <p style={{ margin: "5px 0" }}>
                            {description}
                        </p>
                    </Grid>
                    <Grid container direction="row" alignItems="center">
                        <Grid item xs={1}>
                            <img alt="star icon" src="./favicon.ico" widh="20px" height="20px" />
                        </Grid>
                        <Grid item xs={5}>
                            <span>
                                {stargazers_count}
                            </span>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={4}>

                    <Grid container justify="flex-end">
                        {!unstar ?
                            <Button
                                title="Star"
                                className={(classes.cssRootOutlined)}
                                variant="outlined"
                                onClick={() => this.toggleButton("Star")}
                            >
                                Star
                            </Button>
                            : <Button
                                title="Unstar"
                                className={(classes.margin, classes.cssRoot)}
                                variant="contained"
                                onClick={() => this.toggleButton("Unstar")}
                            >
                                Unstar
                             </Button>
                        }
                    </Grid>
                </Grid>
            </Grid >
        );
    }
}
const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            toggleHeaderAvatar
        },
        dispatch
    );

export default compose(
    withStyles(styles),
    connect(
        null,
        mapDispatchToProps
    )
)(CardItem);
