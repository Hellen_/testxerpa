import React from 'react';

// Material-UI
import { Grid, Input, InputAdornment, Avatar } from "@material-ui/core"
import { Search } from "@material-ui/icons"
import { withStyles } from "@material-ui/core/styles"

// Styles
import style from './headerStyle.css';

const styles = {
    bigAvatar: {
        width: "70px",
        height: "70px",
    },
};
class Header extends React.Component {
    render() {
        const { classes, onChange, value, avatar } = this.props;
        return (
            <Grid container direction="row" alignContent="center" alignItems="center">
                <Grid item xs={12} md={3} lg={2} id="Logo" className={style.logo}>
                    <h4>
                        Github
                        <span>
                            Stars
                        </span>
                    </h4>
                </Grid>
                <Grid item xs={10} md={7} lg={9}>
                    <Input
                        type='text'
                        value={value}
                        onChange={onChange}
                        placeholder="github username"
                        style={{ width: "100%" }}
                        endAdornment={(
                            <InputAdornment position="end">
                                <Search />
                            </InputAdornment>
                        )}
                    />
                </Grid>
                <Grid item xs={2} lg={1}>
                    <Avatar alt="User Avatar" src={avatar} className={classes.bigAvatar} />
                </Grid>
            </Grid>
        );
    }
}

export default withStyles(styles)(Header);