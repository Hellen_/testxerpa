import React, {
    Component
} from 'react';

// Material-UI
import { Grid, Avatar } from "@material-ui/core"
import { withStyles } from "@material-ui/core/styles"
import { LocationOnOutlined, LanguageOutlined, MailOutlined, PeopleOutline } from "@material-ui/icons"

// Style
import style from "./userBarStyle.css"

const styles = {
    bigAvatar: {
        width: "140px",
        height: "140px",
    },
};

class UserBar extends Component {
    render() {
        const { classes, location, avatar, name, login, blog, email, bio, company } = this.props;
        return (
            <Grid container className={style.container}>
                <Grid container className={style.nickUser}>
                    <Grid container justify="center" alignItems="center" >
                        <Avatar
                            alt="User Avatar"
                            src={avatar}
                            className={classes.bigAvatar}
                        />
                    </Grid>
                    <Grid item>
                        <Grid item>
                            <h3 style={{margin: 0, fontWeight:400}}>
                                {name}
                            </h3>
                        </Grid>
                        <Grid item>
                            {login}
                        </Grid>
                    </Grid>
                </Grid>

                <Grid container className={style.infoUser}>
                    <Grid item>
                        {bio}
                    </Grid>
                    <Grid container className={style.generalInfoUser} >
                        {company ?
                            <Grid container direction="row" alignItems="center">
                                <Grid item xs={2} >
                                    <PeopleOutline />
                                </Grid>
                                <Grid item xs={10}>
                                    {company}
                                </Grid>
                            </Grid>
                            : null
                        }
                        {location ?
                            <Grid container direction="row" alignItems="center">
                                <Grid item xs={2}>
                                    <LocationOnOutlined />
                                </Grid>
                                <Grid item xs={10}>
                                    {location}
                                </Grid>
                            </Grid>
                            : null
                        }
                        {email ?
                            <Grid container direction="row" alignItems="center">
                                <Grid item xs={2}>
                                    <MailOutlined />
                                </Grid>
                                <Grid item xs={10}>
                                    {email}
                                </Grid>
                            </Grid>
                            : null
                        }
                        {blog ?
                            <Grid container direction="row" alignItems="center">
                                <Grid item xs={2}>
                                    <LanguageOutlined />
                                </Grid>
                                <Grid item xs={10}>
                                    {blog ? blog.replace("https://", "") : ""}
                                </Grid>
                            </Grid>
                            : null
                        }

                    </Grid>
                </Grid>
            </Grid>

        );
    }
}

export default withStyles(styles)(UserBar);