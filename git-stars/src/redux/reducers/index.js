import handleHeader from "./handleHeader";
import { combineReducers } from "redux";

export default combineReducers({
  handleHeader
});
