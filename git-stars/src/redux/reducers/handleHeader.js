const initialState = {
    linkAvatar: 'https://octodex.github.com/images/femalecodertocat.png'
};

export default function handleHeader(state = initialState, action) {
    switch (action.type) {
        case 'TOGGLE_HEADER_AVATAR':
            return {
                ...state,
                linkAvatar: action.link
            };
        default:
            {
                return {
                    ...state
                };
            }
    }
}