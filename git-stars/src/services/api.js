import axios from "axios"

export var dataGitHub = {
  getByUsername: function (username) {
    return axios.get('https://api.github.com/users/' + username);
  },

  getReposStarred: function (username) {
    return axios.get('https://api.github.com/users/' + username + '/starred');
  },

  putStarredRepos: function (reposit, tokenUser) {
    return axios.put('https://api.github.com/user/starred/' + reposit + '?access_token=' + tokenUser);
  },

  getAuthUser: function (tokenUser) {
    return axios.get('https://api.github.com/user?access_token=' + tokenUser);
  },

  deleteStarredRepos: function (reposit, tokenUser) {
    return axios.delete('https://api.github.com/user/starred/' + reposit + '?access_token=' + tokenUser);
  },

};