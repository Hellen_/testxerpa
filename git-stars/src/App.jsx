import React from 'react';

// Redux
import { connect } from 'react-redux';

// Services
import { dataGitHub } from "./services/api.js"

// Material-UI
import { Grid } from "@material-ui/core"

// Components
import Index from "./containers/index/index"
import Header from "./components/header/header"

class App extends React.Component {
  state = {
    searchFild: "",
    erroContainer: false,
    message: undefined,
    login: [],
    starred: [],
  }

  changeSearchFild = e => {
    this.setState({
      ...this.state,
      searchFild: e.target.value,
      erroContainer: false
    });
    setTimeout(() => this.loadInfos(), 20)
  }

  loadInfos = () => {
    const { searchFild, login } = this.state;
    if (login.name === null) {
      this.setState({
        ...this.state,
        erroContainer: true
      })
    }

    dataGitHub.getByUsername(searchFild).then(function (response) {
      this.setState({
        ...this.state,
        login: response.data,
      })
    }.bind(this))

    dataGitHub.getReposStarred(searchFild).then(function (response) {
      this.setState({
        ...this.state,
        starred: response.data,
      })
    }.bind(this))

    return
  }

  render() {
    const { searchFild, erroContainer, login, starred } = this.state;
    const { linkAvatar } = this.props;

    return (
      <Grid container alignItems="center" justify="center" style={{ height: "100vh" }}>
        <Grid item xs={12} sm={9}>
          <Header
            onChange={this.changeSearchFild}
            value={searchFild}
            avatar={linkAvatar}
          />
          {erroContainer ?
            <Grid container justify="center" direction="column" alignItems="center" style={{ height: "70vh" }}>
              <img src="./images/userNotFound.png" alt="user not found" width="110px" height="110px" />
              <Grid item>
                <h1 style={{ fontWeight: "lighter", color: "gray" }}>User not found</h1>
              </Grid>
            </Grid>
            : searchFild.length > 0
              ? <Index infos={login} starred={starred} />
              : null
          }
        </Grid>
      </Grid>
    );
  }
}


const mapStateToProps = store => ({
  linkAvatar: store.handleHeader.linkAvatar
});

export default connect(mapStateToProps)(App);