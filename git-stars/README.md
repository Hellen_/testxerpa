##Start Project

    -npm install
    -npm run start

    *Caso a aba no navegador não abra automaticamente, digite:
        http://localhost:3000/

___

###Estrutura de pastas:

    -public
    -images
        imageExample
    {arquivos index}
    -src
    -components
        componentExample
            componentExample {index}
            componentExample {style}
    -containers
        containerExample
            containerExample {index}
            containerExample {style}
    -redux
        actions
            actionExample
        reducers
            index
            reduceExample
    -services
        serviceExample
    {arquivos index}
